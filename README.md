# Deluged Paiga #

### CURRENTLY BROKEN, IN PROCESS OF FIXING ###

Deluged Paiga is a placeholder tool for Taiga to add support for Deluge to download anime into their own folder.  
For it to work, you must be running a deluged server instance on a computer you have access to either over the  
internet or locally, or have the deluge local client running.

### How do I get set up? ###

1. Ensure you have either the Deluged Daemon Server running ([instructions can be found here](http://dev.deluge-torrent.org/wiki/UserGuide/ThinClient)) or have the Deluge GUI running.
2. Inside Taiga go to Tools>Settings>Torrents>Downloads and make sure to select "Use a custom application:" as seen here:
![Settings_2015-04-25_06-19-05.png](https://bitbucket.org/repo/69bA7o/images/1293991222-Settings_2015-04-25_06-19-05.png)  
You can put the utorrent.py file anywhere you like, but it must stay named utorrent.py as Taiga looks for utorrent in the path to the custom application.
3. Set up automatic torrent downloading here:  
![Settings_2015-04-25_06-21-55.png](https://bitbucket.org/repo/69bA7o/images/109468283-Settings_2015-04-25_06-21-55.png)  
And you're done!

### Who do I talk to? ###

Devian50, I'm the only admin.